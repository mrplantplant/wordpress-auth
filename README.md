# Goal
Use this package to compare a given password to a WordPress hashed string. My personal use case is when migrating away from WordPress, but don't want our users to have to reset their password. I'd import the users from the WordPress database into my new data structure, with a user type identifier (so we know which user requires which authentication method). On a successful login, the code knows the correct password and can be passed through to your new hashing method. 

Should you need inspiration for a new password hashing method, look at [password_hash()](https://www.php.net/manual/en/function.password-hash.php). Take particular note at [this comment](https://www.php.net/manual/en/function.password-hash.php#124138) to add additional security.

# Usage
Very simple, create a new instance of `WPAuth\WordPressAuth` and use the `authenticate()` method on it as per the following:
```php
<?php
    $authManager = new MrPlantPlant\WPAuth\WordPressAuth();
    
    // Returns true on a successful match, false on mismatch 
    if ($authManager->authenticate($givenPassword, $storedHash)) {
        // Now rehash $givenPassword with your new auth method and save it in your data source!
        echo 'success!'
    } else {
        echo 'password mismatch';
    }
?>
```

