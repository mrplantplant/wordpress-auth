<?php

namespace MrPlantPlant\WPAuth;

class WordPressAuth
{
    private WordPressPhpass $wpHash;

    public function __construct() {
        $this->wpHash = new WordPressPhpass(8, true);
    }

    /**
     * Returns if the given password (e.g. by the user trying to log in) matches the saved hash
     *
     * @param string $givenPassword The password that the user has provided
     * @param string $storedHash The hash stored by WordPress (most likely in your database)
     * @return bool True if password matches hash, false if password is wrong (doesn't match hash)
     */
    public function authenticate(string $givenPassword, string $storedHash): bool {
        return $this->wpHash->CheckPassword($givenPassword, $storedHash);
    }
}